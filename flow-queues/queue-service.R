library(ggplot2)
library(dplyr)

sigma_q=seq(0.6, 1, 0.1)
range=seq(0.6, 1, 0.1)
x = seq(0, 1, 0.01)

df <- NULL

for(i in 1:length(range))
{
  y = x^2/(a[i]-x)
  
  aux <- data.frame('x'=x, 'y'=y, 'sigma_q'=sigma_q[i])
  aux$y = ifelse(aux$x>=range[i], NA, aux$y)
  df <- bind_rows(df, aux)
}

p <- ggplot(df,
    aes(x=x, y=y, group=factor(sigma_q), col=factor(sigma_q))) +
    geom_line() +
    ylim(0, 10)

p + labs(title = "Swimlanes", subtitle = NULL)
p + xlab("Tasso di utilizzo (%)") + ylab("Dimensione della coda")
