// Passo 0: definizione formale
//function multiply() {}

// Step 1: fake
//function multiply() {
//  return 10
//}

// Step 2: implementazione subottimale
//function multiply(a, b) {
//  return a * b;
//}

// Step 3: refactor
const multiply = (a, b) => a * b

// test
const assert = require('assert');
assert.strictEqual(multiply(5, 2), 10);

